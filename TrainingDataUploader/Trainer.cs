﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Azure.Identity;
using Azure.Storage.Blobs;
using Azure.Storage.Blobs.Models;
using Azure.Storage.Sas;
using ByteSizeLib;
using Newtonsoft.Json;
using ShellProgressBar;

namespace TrainingDataUploader
{
    internal class Trainer
    {
        private const string DataLocale = "en-US";
        private const int DataPollTime = 2000; //milliseconds
        private readonly string _datasetName;
        private readonly FileInfo _zipFile;
        private readonly BlobServiceClient _blobServiceClient;
        private readonly Configuration.Trainer _trainerConfig;
        private ProgressBar _progressBar;
        private long _uploadFileSize;

        private enum DataStatus { PROCESSING, SUCCEEDED, FAILED };

        public Trainer(Configuration.Trainer trainerConfig)
        {
            _trainerConfig = trainerConfig;
            _datasetName = $"OverlordBot-{DateTime.Today:yyyy-MM-dd}";
            _zipFile = new FileInfo(_datasetName + ".zip");

            // Construct the blob endpoint from the account name.
            var blobEndpoint = $"https://{_trainerConfig.StorageAccountName}.blob.core.windows.net";
            // Create a new Blob service client with Azure AD credentials.
            _blobServiceClient = new BlobServiceClient(new Uri(blobEndpoint), new InteractiveBrowserCredential());
        }

        public async Task RunTraining()
        {
            var masterSpeechSubscription = _trainerConfig.SpeechKeys[0];

            try
            {
                string trainedModelURI = await CheckModelAlreadyDeployed(masterSpeechSubscription);
                // If endpoint has latest model - Do nothing
                if (trainedModelURI == "null")
                {
                    string newModelURI = await CheckModelAlreadyTrained(masterSpeechSubscription);

                    // else if model exists but not deployed - Only deploy the model
                    if (newModelURI == "null")
                    {
                        string trainingDatasetURI = await CheckDatasetAlreadyInSpeech(masterSpeechSubscription);

                        // else if only dataset exists - Train and deploy model to the existing endpoint
                        if (trainingDatasetURI == "null")
                        {
                            CheckTrainingData();

                            CompressTrainingData(_trainerConfig.TrainingDataFolder);

                            await UploadTrainingData();

                            var blobUri = await GetAccessUri();

                            trainingDatasetURI = await TransferToCustomSpeech(blobUri, masterSpeechSubscription);
                        }

                        else
                        {
                            Console.WriteLine($"|{masterSpeechSubscription.Name}| Dataset has already been uploaded");
                        }

                        newModelURI = await TrainCustomSpeech(trainingDatasetURI, masterSpeechSubscription);

                        await RemoveTrainingDataset(trainingDatasetURI, masterSpeechSubscription);
                    }

                    else
                    {
                        Console.WriteLine($"|{masterSpeechSubscription.Name}| Model has already been trained");
                    }

                    await DeployNewSpeechModel(newModelURI, masterSpeechSubscription);

                    await RemoveOldModels(masterSpeechSubscription);

                    trainedModelURI = newModelURI;
                }

                else
                {
                    Console.WriteLine($"|{masterSpeechSubscription.Name}| Endpoint is up to date");
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                throw new Exception(ex.Message);
            }

            Console.WriteLine($"Starting copy of trained model from {masterSpeechSubscription.Name}");

            for (int i = 0; i < _trainerConfig.SpeechKeys.Count; i++)
            {
                try
                {
                    string trainedModelURI = await CheckModelAlreadyDeployed(_trainerConfig.SpeechKeys[i]);

                    // If endpoint has latest model - Do nothing
                    if (trainedModelURI == "null")
                    {
                        string newModelURI = await CheckModelAlreadyTrained(_trainerConfig.SpeechKeys[i]);

                        // else if model exists but not deployed - Only deploy the model
                        if (newModelURI == "null")
                        {
                            await CopyTrainedModel(trainedModelURI, masterSpeechSubscription, _trainerConfig.SpeechKeys[i]);
                        }

                        else
                        {
                            Console.WriteLine($"|{_trainerConfig.SpeechKeys[i].Name}| Model has already been transferred");
                        }

                        await DeployNewSpeechModel(newModelURI, masterSpeechSubscription);

                        await RemoveOldModels(masterSpeechSubscription);
                    }

                    else
                    {
                        Console.WriteLine($"|{_trainerConfig.SpeechKeys[i].Name}| Endpoint is up to date");
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
            }
        }

        private void CheckTrainingData()
        {
            var audioFiles = Directory.GetFiles(_trainerConfig.TrainingDataFolder).Select(fileName => new FileInfo(fileName)).ToList();
            audioFiles.RemoveAll(file => file.Extension != ".wav");

            var translationEntries = new Dictionary<string, string>();
            var duplicateEntries = new List<string>();
            foreach (var line in File.ReadAllLines(_trainerConfig.TrainingDataFolder + "\\Trans.txt"))
            {
                var name = line.Split('\t').First();
                var text = line.Split('\t').Last();
                if (translationEntries.ContainsKey(name))
                {
                    duplicateEntries.Add(name);
                }
                else
                {
                    translationEntries[name] = text;
                }
            }

            var filesNotInTranslation = audioFiles.Select(file => file.Name).Except(translationEntries.Keys).ToList();
            var filesNotInDirectory = translationEntries.Keys.Except(audioFiles.Select(file => file.Name)).ToList();
            if (filesNotInTranslation.Count == 0 && filesNotInDirectory.Count == 0 && duplicateEntries.Count == 0) return;

            if (duplicateEntries.Count > 0)
            {
                Console.WriteLine("Duplicate file names found in Trans.txt file");
                foreach (var fileName in duplicateEntries)
                {
                    Console.WriteLine(fileName);
                }
            }

            if (filesNotInTranslation.Count > 0)
            {
                Console.WriteLine("Files missing Trans.txt entry");
                Console.WriteLine("Moving files to 'Untrained' directory");
                var untrainedFolder = _trainerConfig.TrainingDataFolder + "/../Untrained";
                foreach (var file in filesNotInTranslation)
                {
                    Console.WriteLine(file);
                    var source = Path.Combine(_trainerConfig.TrainingDataFolder, file);
                    var target = Path.Combine(untrainedFolder, file);
                    File.Move(source, target, true);
                }

            }
            if (filesNotInDirectory.Count > 0)
            {
                Console.WriteLine("Entries in Trans.txt missing in directory");
                foreach (var file in filesNotInDirectory)
                {
                    Console.WriteLine(file);
                }
            }

            Environment.Exit(1);
        }

        private void CompressTrainingData(string folderToCompress)
        {
            if (_zipFile.Exists)
            {
                Console.WriteLine("Zip file already exists");
                return;
            }
            Console.WriteLine("Starting file compression for {0}", folderToCompress);
            try
            {
                ZipFile.CreateFromDirectory(folderToCompress, _zipFile.Name);
            }
            catch (DirectoryNotFoundException ex)
            {
                Console.WriteLine(ex.Message);
                throw new DirectoryNotFoundException(ex.Message);
            }
        }

        private async Task UploadTrainingData()
        {
            try
            {
                Console.WriteLine("Uploading Zip to Azure");

                var containerClient = _blobServiceClient.GetBlobContainerClient(_trainerConfig.ContainerName);
                var blobClient = containerClient.GetBlobClient(_zipFile.Name);

                if (await blobClient.ExistsAsync() == true)
                {
                    Console.WriteLine("Zip already present in Azure");
                    return;
                }

                _uploadFileSize = _zipFile.Length;

                _progressBar = new ProgressBar(100, "Upload File Progress", new ProgressBarOptions
                {
                    ForegroundColor = ConsoleColor.Green,
                    ProgressCharacter = '#'
                });

                var progressHandler = new Progress<long>();
                progressHandler.ProgressChanged += UploadProgressChanged;

                // Open the file and upload its data
                await using var uploadFileStream = File.OpenRead(_zipFile.FullName);
                blobClient.Upload(uploadFileStream, progressHandler: progressHandler);
                uploadFileStream.Close();
                _progressBar.Dispose();
                Console.WriteLine("Zip uploaded to Azure");
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error uploading training data");
                Console.WriteLine(ex.Message);
                Environment.Exit(1);
            }
        }

        private static double GetProgressPercentage(double totalSize, double currentSize)
        {
            return (currentSize / totalSize) * 100;
        }

        private void UploadProgressChanged(object sender, long bytesUploaded)
        {
            _progressBar.Tick((int)GetProgressPercentage(_uploadFileSize, bytesUploaded), $"Uploaded {ByteSize.FromBytes(bytesUploaded).MebiBytes:#.##} MB of {ByteSize.FromBytes(_uploadFileSize).MebiBytes:#.##)} MB");
        }

        private async Task<Uri> GetAccessUri()
        {
            try
            {
                Console.WriteLine("Generating SAS Access URI");
                // Get a user delegation key for the Blob service that's valid for seven days.
                // You can use the key to generate any number of shared access signatures over the lifetime of the key.
                UserDelegationKey key = await _blobServiceClient.GetUserDelegationKeyAsync(DateTimeOffset.UtcNow,
                    DateTimeOffset.UtcNow.AddDays(7));

                // Create a SAS token that's valid for one hour.
                var sasBuilder = new BlobSasBuilder()
                {
                    BlobContainerName = _trainerConfig.ContainerName,
                    BlobName = _zipFile.Name,
                    Resource = "b",
                    StartsOn = DateTimeOffset.UtcNow,
                    ExpiresOn = DateTimeOffset.UtcNow.AddHours(1)
                };

                // Specify read permissions for the SAS.
                sasBuilder.SetPermissions(BlobSasPermissions.Read);

                // Use the key to get the SAS token.
                var sasToken = sasBuilder.ToSasQueryParameters(key, _trainerConfig.StorageAccountName).ToString();

                // Construct the full URI, including the SAS token.
                var fullUri = new UriBuilder()
                {
                    Scheme = "https",
                    Host = $"{_trainerConfig.StorageAccountName}.blob.core.windows.net",
                    Path = $"{_trainerConfig.ContainerName}/{_zipFile}",
                    Query = sasToken
                };
                Console.WriteLine($"SAS Access URI generated: {fullUri.Uri}");
                return fullUri.Uri;
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error generating SAS Access URI");
                Console.WriteLine(ex.Message);
                Environment.Exit(1);
                return null;
            }
        }

        private async Task<string> TransferToCustomSpeech(Uri azureBlobUri, Configuration.SpeechSubscription speechSubscription)
        {
            HttpResponseMessage response = null;
            try
            {
                Console.WriteLine($"|{speechSubscription.Name}| Loading dataset into speech application");

                var formData = new DatasetUploadRequest
                {
                    displayName = _datasetName,
                    description = "",
                    locale = DataLocale,
                    kind = "Acoustic",
                    contentURL = azureBlobUri.ToString(),
                };

                var json = JsonConvert.SerializeObject(formData);
                var data = new StringContent(json, Encoding.UTF8, "application/json");

                var client = new HttpClient();

                // Request headers
                client.DefaultRequestHeaders.Add("Ocp-Apim-Subscription-Key", speechSubscription.SubscriptionKey);

                string uri = $"https://{speechSubscription.Region}.api.cognitive.microsoft.com/speechtotext/v3.0/datasets";

                data.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                response = await client.PostAsync(uri, data);
                //Console.WriteLine(response);

                response.EnsureSuccessStatusCode();

                var datasetURI = JsonConvert.DeserializeObject<SpeechDataResponse>(await response.Content.ReadAsStringAsync())
                    .self;

                var datasetStatus = DataStatus.PROCESSING;

                Console.WriteLine($"|{speechSubscription.Name}| Waiting for data to finish processing");

                while (datasetStatus == DataStatus.PROCESSING)
                {
                    Thread.Sleep(DataPollTime);

                    datasetStatus = await CheckDataStatus(datasetURI, speechSubscription.SubscriptionKey);

                    if (datasetStatus == DataStatus.FAILED)
                    {
                        throw new Exception("Dataset failed processing in speech service!");
                    }
                }

                return datasetURI;
            }
            catch (Exception ex)
            {
                Console.WriteLine("|{0}| {1}", speechSubscription.Name, ex.Message);
                Console.WriteLine("|{0}| {1}", speechSubscription.Name, await response.Content.ReadAsStringAsync());
                throw new Exception($"|{speechSubscription.Name}| Failed to transfer data to speech instance.");
            }
        }

        private static async Task<DataStatus> CheckDataStatus(string datasetURI, string subscriptionKey)
        {
            try
            {
                var client = new HttpClient();
                client.DefaultRequestHeaders.Add("Ocp-Apim-Subscription-Key", subscriptionKey);

                var response = await client.GetAsync(datasetURI);

                response.EnsureSuccessStatusCode();

                var datasetStatus = JsonConvert.DeserializeObject<SpeechDataResponse>(await response.Content.ReadAsStringAsync())
                    .status;

                switch (datasetStatus)
                {
                    case "Succeeded":
                        return DataStatus.SUCCEEDED;
                    case "Processing":
                        return DataStatus.PROCESSING;
                    case "NotStarted":
                        return DataStatus.PROCESSING;
                    case "Running":
                        return DataStatus.PROCESSING;
                    case "Failed":
                        return DataStatus.FAILED;
                    default:
                        throw new Exception("Dataset status contained invalid response.");
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error checking training dataset status");
                Console.WriteLine(ex.Message);
                throw new Exception($"Failed to check training dataset status.");
            }
        }

        private async Task<string> TrainCustomSpeech(string trainingDataset, Configuration.SpeechSubscription speechSubscription)
        {
            HttpResponseMessage response = null;
            try
            {
                // TODO: Find a way to find the latest base model by calling /speechtotext/v3.0/models/base?skip[number]&top[number]
                // Latest base model as of 12/11/20: 3af2f4f9-a3d6-4036-ae26-ff044a8c6592
                Console.WriteLine($"|{speechSubscription.Name}| Creating training model from dataset");

                var requestData = new ModelCreationRequest
                {
                    displayName = _datasetName,
                    locale = DataLocale,
                    baseModel = new ModelCreationRequestData { self = $"https://{speechSubscription.Region}.api.cognitive.microsoft.com/speechtotext/v3.0/models/base/3af2f4f9-a3d6-4036-ae26-ff044a8c6592" },
                    datasets = new List<ModelCreationRequestData> { new ModelCreationRequestData { self = trainingDataset } }
                };

                var json = JsonConvert.SerializeObject(requestData);
                var data = new StringContent(json, Encoding.UTF8, "application/json");

                var client = new HttpClient();

                // Request headers
                client.DefaultRequestHeaders.Add("Ocp-Apim-Subscription-Key", speechSubscription.SubscriptionKey);

                string uri = $"https://{speechSubscription.Region}.api.cognitive.microsoft.com/speechtotext/v3.0/models";

                data.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                response = await client.PostAsync(uri, data);

                //Console.WriteLine(response.Content.ReadAsStringAsync().Result);

                response.EnsureSuccessStatusCode();

                var modelURI = JsonConvert.DeserializeObject<SpeechDataResponse>(await response.Content.ReadAsStringAsync())
                    .self;

                var datasetStatus = DataStatus.PROCESSING;

                Console.WriteLine($"|{speechSubscription.Name}| Waiting for model to finish training");

                while (datasetStatus == DataStatus.PROCESSING)
                {
                    Thread.Sleep(DataPollTime);

                    datasetStatus = await CheckDataStatus(modelURI, speechSubscription.SubscriptionKey);

                    if (datasetStatus == DataStatus.FAILED)
                    {
                        throw new Exception("Model failed training in speech service!");
                    }
                }

                return modelURI;
            }
            catch(Exception ex)
            {
                Console.WriteLine("|{0}| {1}", speechSubscription.Name, ex.Message);
                Console.WriteLine("|{0}| {1}", speechSubscription.Name, await response.Content.ReadAsStringAsync());
                throw new Exception($"|{speechSubscription.Name}| Failed to train speech instance.");
            }
        }

        private async Task RemoveTrainingDataset(string trainingDataset, Configuration.SpeechSubscription speechSubscription)
        {
            HttpResponseMessage response = null;
            try
            {
                Console.WriteLine($"|{speechSubscription.Name}| Removing training dataset");

                var client = new HttpClient();
                client.DefaultRequestHeaders.Add("Ocp-Apim-Subscription-Key", speechSubscription.SubscriptionKey);

                response = await client.DeleteAsync(trainingDataset);

                response.EnsureSuccessStatusCode();
            }
            catch (Exception ex)
            {
                Console.WriteLine("|{0}| {1}", speechSubscription.Name, ex.Message);
                Console.WriteLine("|{0}| {1}", speechSubscription.Name, await response.Content.ReadAsStringAsync());
                throw new Exception($"|{speechSubscription.Name}| Failed to remove training dataset.");
            }
        }

        private async Task<string> DeployNewSpeechModel(string newModelURI, Configuration.SpeechSubscription speechSubscription)
        {
            HttpResponseMessage response = null;
            try
            {
                Console.WriteLine($"|{speechSubscription.Name}| Deploying new model");

                var getClient = new HttpClient();
                getClient.DefaultRequestHeaders.Add("Ocp-Apim-Subscription-Key", speechSubscription.SubscriptionKey);
                var getResponse = await getClient.GetStringAsync($"https://{speechSubscription.Region}.api.cognitive.microsoft.com/speechtotext/v3.0/endpoints/");

                var endpointList = JsonConvert.DeserializeObject<SpeechListResponse>(getResponse);

                var requestData = new EndpointUpdateRequest
                {
                    displayName = _datasetName,
                    model = new ModelCreationRequestData { self = newModelURI }
                };

                var json = JsonConvert.SerializeObject(requestData);
                var data = new StringContent(json, Encoding.UTF8, "application/json");

                var client = new HttpClient();

                // Request headers
                client.DefaultRequestHeaders.Add("Ocp-Apim-Subscription-Key", speechSubscription.SubscriptionKey);

                // This assumes each instance only has a single endpoint
                var uri = endpointList.values[0].self;

                data.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                response = await client.PatchAsync(uri, data);

                response.EnsureSuccessStatusCode();

                return JsonConvert.DeserializeObject<SpeechDataResponse>(await response.Content.ReadAsStringAsync()).self;
            }
            catch(Exception ex)
            {
                Console.WriteLine("|{0}| {1}", speechSubscription.Name, ex.Message);
                Console.WriteLine("|{0}| {1}", speechSubscription.Name, await response.Content.ReadAsStringAsync());
                throw new Exception($"|{speechSubscription.Name}| Failed to deploy new speech model.");
            }
        }

        private async Task RemoveOldModels(Configuration.SpeechSubscription speechSubscription)
        {
            HttpResponseMessage response = null;
            try
            {
                Console.WriteLine($"|{speechSubscription.Name}| Removing old models");
                var getClient = new HttpClient();
                getClient.DefaultRequestHeaders.Add("Ocp-Apim-Subscription-Key", speechSubscription.SubscriptionKey);
                var getResponse = await getClient.GetStringAsync($"https://{speechSubscription.Region}.api.cognitive.microsoft.com/speechtotext/v3.0/models/");

                var modelList = JsonConvert.DeserializeObject<SpeechListResponse>(getResponse);

                var client = new HttpClient();

                // Request headers
                client.DefaultRequestHeaders.Add("Ocp-Apim-Subscription-Key", speechSubscription.SubscriptionKey);

                foreach (var model in modelList.values)
                {
                    if (model.displayName != _datasetName)
                    {
                        string uri = model.self;
                        response = await client.DeleteAsync(uri);
                        response.EnsureSuccessStatusCode();
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("|{0}| {1}", speechSubscription.Name, ex.Message);
                Console.WriteLine("|{0}| {1}", speechSubscription.Name, await response.Content.ReadAsStringAsync());
                throw new Exception($"|{speechSubscription.Name}| Failed to deploy new speech model.");
            }
        }

        private async Task CopyTrainedModel(string trainedModelURI, Configuration.SpeechSubscription src, Configuration.SpeechSubscription dst)
        {
            HttpResponseMessage response = null;
            try
            {
                Console.WriteLine($"|{dst.Name}| Copying trained model from {src.Name}");

                var requestData = new CopyModelRequest
                {
                    targetSubscriptionKey = dst.SubscriptionKey
                };

                var json = JsonConvert.SerializeObject(requestData);
                var data = new StringContent(json, Encoding.UTF8, "application/json");

                var client = new HttpClient();

                // Request headers
                client.DefaultRequestHeaders.Add("Ocp-Apim-Subscription-Key", src.SubscriptionKey);

                string uri = $"{trainedModelURI}/copyto";

                data.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                response = await client.PostAsync(uri, data);

                response.EnsureSuccessStatusCode();
                var transferredModelURI = JsonConvert.DeserializeObject<SpeechDataResponse>(await response.Content.ReadAsStringAsync()).self;

                var datasetStatus = DataStatus.PROCESSING;

                Console.WriteLine($"|{dst.Name}| Waiting for model to finish transferring");

                while (datasetStatus == DataStatus.PROCESSING)
                {
                    Thread.Sleep(DataPollTime);

                    datasetStatus = await CheckDataStatus(transferredModelURI, dst.SubscriptionKey);

                    if (datasetStatus == DataStatus.FAILED)
                    {
                        throw new Exception("Model failed transferring in speech service!");
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("|{0}| {1}", dst.Name, ex.Message);
                Console.WriteLine("|{0}| {1}", dst.Name, await response.Content.ReadAsStringAsync());
                throw new Exception($"|{dst.Name}| Failed to copy model from {src.Name}.");
            }
        }

        private async Task<string> CheckDatasetAlreadyInSpeech(Configuration.SpeechSubscription speechSubscription)
        {
            HttpResponseMessage response = null;
            try
            {
                var getClient = new HttpClient();
                getClient.DefaultRequestHeaders.Add("Ocp-Apim-Subscription-Key", speechSubscription.SubscriptionKey);
                response = await getClient.GetAsync($"https://{speechSubscription.Region}.api.cognitive.microsoft.com/speechtotext/v3.0/datasets/");
                response.EnsureSuccessStatusCode();

                var datasetList = JsonConvert.DeserializeObject<SpeechListResponse>(await response.Content.ReadAsStringAsync());

                foreach (var dataset in datasetList.values)
                {
                    if (dataset.displayName == _datasetName)
                    {
                        return dataset.self;
                    }
                }

                return "null";
            }
            catch (Exception ex)
            {
                Console.WriteLine("|{0}| {1}", speechSubscription.Name, ex.Message);
                Console.WriteLine("|{0}| {1}", speechSubscription.Name, await response.Content.ReadAsStringAsync());
                throw new Exception($"|{speechSubscription.Name}| Failed to deploy new speech model.");
            }
        }

        private async Task<string> CheckModelAlreadyTrained(Configuration.SpeechSubscription speechSubscription)
        {
            HttpResponseMessage response = null;
            try
            {
                var getClient = new HttpClient();
                getClient.DefaultRequestHeaders.Add("Ocp-Apim-Subscription-Key", speechSubscription.SubscriptionKey);
                response = await getClient.GetAsync($"https://{speechSubscription.Region}.api.cognitive.microsoft.com/speechtotext/v3.0/models/");
                response.EnsureSuccessStatusCode();

                var modelList = JsonConvert.DeserializeObject<SpeechListResponse>(await response.Content.ReadAsStringAsync());

                foreach (var model in modelList.values)
                {
                    if (model.displayName == _datasetName)
                    {
                        return model.self;
                    }
                }

                return "null";
            }
            catch (Exception ex)
            {
                Console.WriteLine("|{0}| {1}", speechSubscription.Name, ex.Message);
                Console.WriteLine("|{0}| {1}", speechSubscription.Name, await response.Content.ReadAsStringAsync());
                throw new Exception($"|{speechSubscription.Name}| Failed to deploy new speech model.");
            }
        }

        private async Task<string> CheckModelAlreadyDeployed(Configuration.SpeechSubscription speechSubscription)
        {
            HttpResponseMessage response = null;
            try
            {
                var getClient = new HttpClient();
                getClient.DefaultRequestHeaders.Add("Ocp-Apim-Subscription-Key", speechSubscription.SubscriptionKey);
                response = await getClient.GetAsync($"https://{speechSubscription.Region}.api.cognitive.microsoft.com/speechtotext/v3.0/endpoints/");
                response.EnsureSuccessStatusCode();

                var endpointList = JsonConvert.DeserializeObject<EndpointListResponse>(await response.Content.ReadAsStringAsync());

                foreach (var endpoint in endpointList.values)
                {
                    if (endpoint.displayName == _datasetName)
                    {
                        return endpoint.model.self;
                    }
                }

                return "null";
            }
            catch (Exception ex)
            {
                Console.WriteLine("|{0}| {1}", speechSubscription.Name, ex.Message);
                Console.WriteLine("|{0}| {1}", speechSubscription.Name, await response.Content.ReadAsStringAsync());
                throw new Exception($"|{speechSubscription.Name}| Failed to deploy new speech model.");
            }
        }
    }

    internal class DatasetUploadRequest
    {
        public string displayName { get; set; }
        public string description { get; set; }
        public string locale { get; set; }
        public string kind { get; set; }
        public string contentURL { get; set; }
    }

    internal class SpeechDataResponse
    {
        public string self { get; set; }
        public string status { get; set; }
        public string displayName { get; set; }
    }

    internal class EndpointResponse : SpeechDataResponse
    {
        public ModelCreationRequestData model { get; set; }
    }

    internal class ModelCreationRequest
    {
        public ModelCreationRequestData baseModel { get; set; }
        public List<ModelCreationRequestData> datasets { get; set; }
        public string locale { get; set; }
        public string displayName { get; set; }
    }

    internal class ModelCreationRequestData
    {
        public string self { get; set; }
    }

    internal class EndpointUpdateRequest
    {
        public ModelCreationRequestData model { get; set; }
        public string displayName { get; set; }
    }

    internal class SpeechListResponse
    {
        public List<SpeechDataResponse> values { get; set; }
    }

    internal class EndpointListResponse
    {
        public List<EndpointResponse> values { get; set; }
    }

    internal class CopyModelRequest
    {
        public string targetSubscriptionKey { get; set; }
    }
}
