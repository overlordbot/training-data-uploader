﻿using System;
using System.IO;
using System.Threading.Tasks;

namespace TrainingDataUploader
{
    internal class Program
    {
        private static async Task<int> Main(string[] args)
        {
            Console.WriteLine("Reading configuration file...");
            string yamlString;
            try
            {
                yamlString = await File.ReadAllTextAsync("configuration.yaml");
            }
            catch (FileNotFoundException ex)
            {
                Console.WriteLine(ex.Message);
                return 1;
            }

            var trainerConfig = Configuration.Parse(yamlString);

            var trainer = new Trainer(trainerConfig);

            await trainer.RunTraining();

            return 0;
        }
    }
}
