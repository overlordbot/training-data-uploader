# Training Data Uploader

Takes given data for training Azure Speech applications and applies it to all the subscriptions listed in the config file.

## Usage

`./TrainingDataUploader <training data folder>`
